#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require_relative '../tools/tools'

def str_date(time_stamp = DateTime.now)
  # ...
  time_stamp.to_s
end

def ask_company_id(name)
  query = 'SELECT id FROM companies WHERE name=?'
  company = Tools.sqlite_select(@db_name, query, [name])[0]
  company['id'] unless company.nil?
end

def ask_location_id(name)
  query = 'SELECT id FROM locations WHERE name=?'
  location = Tools.sqlite_select(@db_name, query, [name])[0]
  location['id'] unless location.nil?
end

def add_company(name)
  puts name if @verbose
  query = 'INSERT INTO companies(name,created_at,updated_at)values(?,?,?)'
  dt = DateTime.now.to_time.to_i
  Tools.sqlite_insert(@db_name, query, [name, dt, dt])
  ask_company_id(name)
end

def add_location(name)
  puts name if @verbose
  query = 'INSERT INTO locations(name,created_at,updated_at)values(?,?,?)'
  dt = DateTime.now.to_time.to_i
  Tools.sqlite_insert(@db_name, query, [name, dt, dt])
  ask_location_id(name)
end

def get_company_id(name)
  id = ask_company_id(name)
  id = add_company(name) if id.nil?
  id
end

def get_location_id(name)
  id = ask_location_id(name)
  id = add_location(name) if id.nil?
  id
end

def add_record(data)
  query = 'INSERT INTO tels(prefix,num_start,num_finish,amount,company,geo,' \
    'created_at,updated_at)values(?,?,?,?,?,?,?,?)'
  company_id = get_company_id(data[:company])
  location_id = get_location_id(data[:geo])
  dt = DateTime.now.to_time.to_i
  params = [data[:prefix], data[:num_start], data[:num_finish], data[:amount],
            company_id, location_id, dt, dt]
  Tools.sqlite_insert(@db_name, query, params)
end

def add_db(source_file)
  # fill DataBase with data
  data = Tools.read_textfile(source_file)
  data.each do |line|
    rec = line.chomp.split(';')
    data = {
      prefix: rec[0], num_start: rec[1], num_finish: rec[2],
      amount: rec[3], company: rec[4], geo: rec[5]
    }
    # STDIN.gets.chomp
    add_record(data)
  end
end

def update_source
  url = 'https://rossvyaz.ru/data/'
  @sources.each { |s| Tools.download_file(url + s) }
end

def create_db
  # Create db with tables
  sql = Tools.read_textfile('tels.sql').join('')
  queries = sql.chomp.split(';')
  queries.each do |query|
    Tools.sqlite_insert(@db_name, query, [])
  end
end

def fill_db
  @sources.each { |s| add_db(s) }
end

################################################################################
##  https://rossvyaz.ru/data/{ABC-3xx.csv,ABC-4xx.csv,ABC-8xx.csv,DEF-9xx.csv}
################################################################################
@db_name = '/mnt/ramdisk/tels.db'
# @db_name = 'tels.db'
@sources = %w[ABC-3xx.csv ABC-4xx.csv ABC-8xx.csv DEF-9xx.csv]
@verbose = false

ds = DateTime.now

@verbose = true if ARGV.include?('-v')

if ARGV.include?('-u')
  Dir.chdir File.dirname(File.absolute_path($PROGRAM_NAME))
  puts Dir.pwd
  system('rm -f *.csv && sync')
  update_source
  system('md5sum -c md5sum.txt')
  # system('mv md5sum.txt md5sum.old')
  # system('md5sum *.csv > md5sum.txt')
end

if ARGV.include?('-r')
  create_db
  fill_db
end

df = DateTime.now
puts 'Time start:     ' + ds.to_s
puts 'Time finish:    ' + df.to_s
time = df.strftime('%s').to_i - ds.strftime('%s').to_i
puts "Operation time: #{(time / 60)}:#{(time % 60)}"
system("ls -lach #{@db_name}") if File.exist?(@db_name)
