DROP TABLE IF EXISTS companies;
CREATE TABLE companies (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  name TEXT NOT NULL,
  created_at DATETIME,
  updated_at DATETIME
);
CREATE UNIQUE INDEX companies_idx on companies (name);

DROP TABLE IF EXISTS locations;
CREATE TABLE locations (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  name TEXT NOT NULL,
  created_at DATETIME,
  updated_at DATETIME
);
CREATE UNIQUE INDEX locations_idx on locations (name);

DROP TABLE IF EXISTS tels;
CREATE TABLE tels (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  prefix INTEGER NOT NULL,
  num_start INTEGER NOT NULL,
  num_finish INTEGER NOT NULL,
  amount INTEGER NOT NULL,
  company INTEGER NOT NULL,
  geo INTEGER NOT NULL,
  created_at INTEGER,
  updated_at INTEGER
);
